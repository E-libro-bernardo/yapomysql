﻿using System;
using System.Data;
using MySql.Data.MySqlClient;
using System.Collections.Generic;

using System.Collections;
using System.Text;
/// <summary>
/// Summary description for MysqlMain
/// </summary>
namespace YapoMysqlTool
{
    public enum yOperator
    {
        Equal,
        Less,
        Bigger,
        LessEqual,
        BiggerEqual,
        NotEqual,
        IsNull
    }
    public abstract class ybase : IDisposable
    {
        static Random random = new Random();
        private string _ConnectionString = "";
        protected string linebreak = "\r\n";
        public string ConnectionString { get { return _ConnectionString; } set { _ConnectionString = value; } }
        public virtual void Dispose()
        {

        }

        protected static int randomNumber()
        {
            return randomNumber(1, 10000000);
        }

        //generate random numbers overloaded
        protected static int randomNumber(int min, int max)
        {
            int mvalue;

            mvalue = random.Next(min, max);

            return mvalue;

        }

    }

    sealed public class ySQLE : ybase
    {
        ~ySQLE()
        {


        }
        public override void Dispose()
        {
            _paramname.Clear();
            _paramvalue.Clear();

            ClearMysql();
        }
        private void ClearMysql()
        {
            if (_reader != null)
            {
                _reader.Close();
                _reader.Dispose();
            }
            if (MyCon != null)
            {
                MyCon.Close();
                MyCon.Dispose();
            }


        }
        private int _rowafected = 0;
        public int rowsAffected { get { return _rowafected; } private set { _rowafected = value; } }
        public int RegistrosAfectados { get { return _rowafected; } private set { _rowafected = value; } }
        private int _errcount = 0;
        private MySqlDataReader _reader;
        private ArrayList _errors = new ArrayList();
        private string[] _fields;
        private string[] _fieldsType;
        private int _totFields = 0;//reset this in the main costructor;
        private int _totRows = 0;//reset this in the main costructor;

        private bool _totRowscounted = false;
        MySqlConnection MyCon;//reset this in the main costructor;
        private ArrayList _paramname = new ArrayList();
        private ArrayList _paramvalue = new ArrayList();


        private string msqlcommand;

        private static string staticmerror = "";
        private string _error;
        private bool _iserror = false;

        public bool HasRows
        {
            get
            {

                if (_reader == null)
                {
                    if (!OpenReader())
                    {
                        return false;
                    }

                }
                return _reader.HasRows;
            }

        }
        public string[] Fields
        {

            get
            {
                if (_totFields == 0)
                {
                    OpenReader();

                }
                return _fields;
            }

        }

        public string[] FieldsTypes
        {

            get
            {
                if (_totFields == 0)
                {
                    OpenReader();

                }
                return _fieldsType;
            }

        }

        public string FieldType(string mname)
        {
            string mreturn =
                "";
            if (_totFields == 0)
            {
                OpenReader();

            }

            for (int x = 0; x < _fields.Length; x++)
            {
                if (_fields[x].ToString().ToLower() == mname.ToLower())
                {
                    mreturn = _fieldsType[x].ToString();
                }
            }
            return mreturn;

        }
        public string FieldType(int mposition)
        {
            string mreturn =
                "";
            if (_totFields == 0)
            {
                OpenReader();

            }
            try
            {
                mreturn = _fieldsType[mposition].ToString();
            }

            catch
            {

            }

            return mreturn;

        }
        public string FieldName(int mposition)
        {
            string mreturn =
                "";
            if (_totFields == 0)
            {
                OpenReader();

            }
            try
            {
                mreturn = _fields[mposition].ToString();
            }
            catch
            {

            }

            return mreturn;

        }
        public bool IsError
        {
            get
            {
                return _iserror;
            }
            private set { _iserror = value; }
        }
        public string Error
        {
            get
            {
                string mtemp = "";
                foreach (string mmerror in _errors)
                {
                    mtemp += mmerror + linebreak + linebreak;
                }

                return mtemp;
            }
            private set
            {
                _error = value;
                if (value.Length > 2)
                {
                    _errcount++;
                    _errors.Add(" (Err # " + _errcount.ToString() + " )" + value);
                }


            }
        }
        public string Info
        {
            /// <summary>
            /// Description for SomeMethod.</summary>

            get
            {
                string mreturn = "";
                string mbreak = linebreak + "============>" + linebreak;
                mreturn += string.Format("{0:MM/dd/yyyy HH:mm:ss}", DateTime.Now) + mbreak;
                mreturn += "ERROR =" + (IsError ? "yes" : "No") + mbreak;
                if (IsError)
                {
                    mreturn += Error + mbreak;

                }
                else
                {
                    mreturn += SqlInfo + mbreak;

                }
                mreturn += "DATABASE =";
                mreturn += "CONNECTIONSTRING =" + ConnectionString;
                mreturn += "CONNECTION TO DATABASE = ";
                if (MyCon != null)
                {
                    mreturn += MyCon.State + mbreak;
                }
                else
                {
                    mreturn += "Closed/null" + mbreak;
                }
                mreturn += "READER = ";

                if (_reader != null)
                {
                    mreturn += _reader.IsClosed ? "Closed" : "Open";
                    if (!_reader.IsClosed)
                    {
                        mreturn += ", HASROW=" + _reader.HasRows.ToString() + ", TOTALFIELD =" + _totFields.ToString() + ", TOTALROWS =" + TotalRows.ToString() + mbreak;
                        int xcount = 0;

                        if (_totFields > 0)
                        {

                            mreturn += "FIELD LIST" + mbreak;
                            foreach (string mfield in _fields)
                            {
                                mreturn += linebreak + "Field #" + xcount++.ToString() + " = " + mfield.ToUpper() + " , Type =" + FieldType(mfield);

                            }


                        }

                    }
                    mreturn += linebreak;
                }
                else
                {
                    mreturn += "Closed/null";
                    mreturn += ", TotalRows =" + TotalRows.ToString();
                }




                return mreturn;
            }

        }
        public int ErrorCount
        {
            get { return _errcount; }
        }
        public string[] Errors
        {
            get
            {
                string[] _errstring2 = new string[_errors.Count];
                for (int x = 0; x < _errors.Count; x++)
                {
                    _errstring2[x] = " (Error #" + (x + 1).ToString() + " ) =" + _errors[x].ToString() + linebreak;
                }
                return _errstring2;
            }
        }


        public static string ErrorStatic
        {
            get
            {

                return staticmerror;
            }

        }
        public bool IsokConnection
        {
            get
            {
                bool mreturn = true;

                MySqlConnection mcontest = new MySqlConnection(ConnectionString);
                try
                {
                    mcontest.Open();
                }

                catch (Exception ex)
                {
                    staticmerror = ex.Message;
                    mreturn = false;

                }
                finally
                {
                    mcontest.Close();
                    mcontest = null;


                }
                return mreturn;
            }
        }

        //use this to do with multiple parameter

        public string SetSql
        {
            get
            {
                return msqlcommand;
            }
            set
            {
                msqlcommand = value;

            }
        }
        public ySQLE()
        {


        }
        public ySQLE(string ConnectionStringValue)
        {
            ConnectionString = ConnectionStringValue;
        }

        public ySQLE(string ConnectionStringValue, string sqlvalue, ySQLP mlist)
            : this()
        {
            ConnectionString = ConnectionStringValue;
            SetSql = sqlvalue;
            for (int x = 0; x < mlist.Count; x++)
            {
                AddParameter(mlist.Fields[x].ToString(), mlist.Values[x]);
            }
            ;
        }

        public ySQLE(string ConnectionStringValue, string sqlvalue)
            : this()
        {
            ConnectionString = ConnectionStringValue;
            SetSql = sqlvalue;

        }
        //this is overloaded with one paramenter
        public ySQLE(string ConnectionStringValue, string sqlvalue, string parameter1, string value1)
            : this()
        {
            ConnectionString = ConnectionStringValue;
            SetSql = sqlvalue;

            AddParameter(parameter1, value1);

        }
        public ySQLE(string ConnectionStringValue, string sqlvalue, List<string> Lpara, List<string> Lvalues)
            : this()
        {
            string tmperror = "";

            if (Lpara.Count == 0)
            {
                tmperror += "<error>Lista de parametros vacia</error>";
            }
            if (Lvalues.Count == 0)
            {
                tmperror += "<error>Lista de valores de parametro vacia</error>";
            }
            if (tmperror != "")
            {
                Error = tmperror;
                return;
            }
            if (Lpara.Count != Lvalues.Count)
            {
                Error = "<error>Cantidad de pramentros y valores no son iguales</error>";
                return;

            }
            ConnectionString = ConnectionStringValue;
            SetSql = sqlvalue;
            for (int x = 0; x < Lpara.Count; x++)
            {
                AddParameter(Lpara[x], Lvalues[x]);
            }
        }


        public ySQLE(string ConnectionStringValue, string sqlvalue, string[] ArrayParameters, string[] ArrayValues)
            : this()
        {
            string tmperror = "";

            if (ArrayParameters.Length == 0)
            {
                tmperror += "<error>Lista de parametros vacia</error>";
            }
            if (ArrayValues.Length == 0)
            {
                tmperror += "<error>Lista de valores de parametro vacia</error>";
            }
            if (tmperror != "")
            {
                Error = tmperror;
                return;
            }
            if (ArrayParameters.Length != ArrayValues.Length)
            {
                Error = "<error>Cantidad de pramentros y valores no son iguales</error>";
                return;

            }
            ConnectionString = ConnectionStringValue;
            SetSql = sqlvalue;
            for (int x = 0; x < ArrayParameters.Length; x++)
            {
                AddParameter(ArrayParameters[x], ArrayValues[x]);
            }
        }



        public ySQLE(string ConnectionStringValue, string sqlvalue, string parameter1, int value1)
            : this()
        {
            ConnectionString = ConnectionStringValue; SetSql = sqlvalue;
            AddParameter(parameter1, value1);

        }

        public ySQLE(string ConnectionStringValue, string sqlvalue, string parameter1, DateTime value1)
            : this()
        {
            ConnectionString = ConnectionStringValue; SetSql = sqlvalue;
            AddParameter(parameter1, value1);

        }
        public ySQLE(string ConnectionStringValue, string sqlvalue, string parameter1, byte[] value1)
            : this()
        {
            ConnectionString = ConnectionStringValue; SetSql = sqlvalue;
            AddParameter(parameter1, value1);

        }
        public ySQLE(string ConnectionStringValue, string sqlvalue, string parameter1, double value1)
            : this()
        {
            ConnectionString = ConnectionStringValue; SetSql = sqlvalue;
            AddParameter(parameter1, value1);

        }


        //this is overloaded with two paramenter
        public ySQLE(string ConnectionStringValue, string sqlvalue, string parameter1, string value1, string parameter2, string value2)
            : this()
        {
            ConnectionString = ConnectionStringValue; SetSql = sqlvalue;
            AddParameter(parameter1, value1);
            AddParameter(parameter2, value2);
        }

        public ySQLE(string ConnectionStringValue, string sqlvalue, string parameter1, string value1, string parameter2, int value2)
            : this()
        {
            ConnectionString = ConnectionStringValue; SetSql = sqlvalue;
            AddParameter(parameter1, value1);
            AddParameter(parameter2, value2);
        }
        public ySQLE(string ConnectionStringValue, string sqlvalue, string parameter1, int value1, string parameter2, int value2)
            : this()
        {
            ConnectionString = ConnectionStringValue; SetSql = sqlvalue;
            AddParameter(parameter1, value1);
            AddParameter(parameter2, value2);
        }
        public ySQLE(string ConnectionStringValue, string sqlvalue, string parameter1, string value1, string parameter2, DateTime value2)
            : this()
        {
            ConnectionString = ConnectionStringValue; SetSql = sqlvalue;
            AddParameter(parameter1, value1);
            AddParameter(parameter2, value2);
        }
        public ySQLE(string ConnectionStringValue, string sqlvalue, string parameter1, int value1, string parameter2, DateTime value2)
            : this()
        {
            ConnectionString = ConnectionStringValue; SetSql = sqlvalue;
            AddParameter(parameter1, value1);
            AddParameter(parameter2, value2);
        }

        public ySQLE(string ConnectionStringValue, string sqlvalue, string parameter1, string value1, string parameter2, string value2, string parameter3, string value3)
            : this()
        {
            ConnectionString = ConnectionStringValue; SetSql = sqlvalue;
            AddParameter(parameter1, value1);
            AddParameter(parameter2, value2);
            AddParameter(parameter3, value3);
        }
        public ySQLE(string ConnectionStringValue, string sqlvalue, string parameter1, string value1, string parameter2, int value2, string parameter3, string value3)
            : this()
        {
            ConnectionString = ConnectionStringValue; SetSql = sqlvalue;
            AddParameter(parameter1, value1);
            AddParameter(parameter2, value2);
            AddParameter(parameter3, value3);
        }
        public ySQLE(string ConnectionStringValue, string sqlvalue, string parameter1, string value1, string parameter2, int value2, string parameter3, int value3)
            : this()
        {
            ConnectionString = ConnectionStringValue; SetSql = sqlvalue;
            AddParameter(parameter1, value1);
            AddParameter(parameter2, value2);
            AddParameter(parameter3, value3);
        }
        public ySQLE(string ConnectionStringValue, string sqlvalue, string parameter1, int value1, string parameter2, int value2, string parameter3, int value3)
            : this()
        {
            ConnectionString = ConnectionStringValue; SetSql = sqlvalue;
            AddParameter(parameter1, value1);
            AddParameter(parameter2, value2);
            AddParameter(parameter3, value3);
        }
        public ySQLE(string ConnectionStringValue, string sqlvalue, string parameter1, string value1, string parameter2, string value2, string parameter3, string value3, string parameter4, string value4)
            : this()
        {
            ConnectionString = ConnectionStringValue;
            SetSql = sqlvalue;
            AddParameter(parameter1, value1);
            AddParameter(parameter2, value2);
            AddParameter(parameter3, value3);
            AddParameter(parameter4, value4);
        }

        public void Limit(int mlimitvalue)
        {
            SetSql = SetSql + " Limit " + mlimitvalue.ToString();

        }
        public void Limit(int mlimitvalue1, int mlimitvalue2)
        {
            SetSql = SetSql + " Limit " + mlimitvalue1.ToString() + "," + mlimitvalue2.ToString();

        }

        public void AddParameter<T>(string mparamname, T mvalue)
        {

            if (mparamname.Substring(0, 1) != "?")
            {
                mparamname = "?" + mparamname;
            }
            _paramname.Add(mparamname);
            _paramvalue.Add(mvalue);

        }

        //this is to get the whole information of the job to perform and I use to save in the error table 
        public string SqlInfo
        {
            get
            {
                string mreturn = linebreak + "SQL Command :" + linebreak + SetSql + linebreak + "Paramerters:";
                for (int x = 0; x < _paramvalue.Count; x++)
                {
                    mreturn += linebreak + "name =" + _paramname[x].ToString() + " , value=" + _paramvalue[x].ToString();
                }
                return mreturn;
            }
        }

        public bool Execute()
        {

            bool mresult = false;
            try
            {
                MySqlConnection MyContemp = new MySqlConnection(ConnectionString);

                MySqlCommand MyCommand = new MySqlCommand();

                for (int x = 0; x < _paramname.Count; x++)
                {
                    MyCommand.Parameters.AddWithValue(_paramname[x].ToString(), _paramvalue[x]);
                }

                if (MyCommand.Parameters.Count > 0)
                {
                    MyContemp.Open();
                    MyCommand.CommandText = SetSql;
                    MyCommand.Connection = MyContemp;
                    _rowafected = MyCommand.ExecuteNonQuery();

                    MyCommand.CommandText = "";
                    MyCommand.Connection = null;


                    mresult = true;

                }
                else
                {
                    Error = "Cannot use this with out parameters";
                    _iserror = true;
                }
                MyContemp.Close();
                MyContemp.Dispose();
                MyContemp = null;




            }
            catch (MySqlException e)
            {

                Error = mresult + linebreak + e.Message + linebreak + e.Source + linebreak + e.StackTrace
                    + linebreak + e.StackTrace + linebreak + SqlInfo;
                _iserror = true;

            }
            catch (Exception e)
            {

                Error = mresult + linebreak + e.Message + linebreak + e.Source;
                _iserror = true;

            }

            return mresult;
        }


        public int Rows
        {
            get
            {
                return TotalRows;
            }
        }

        public System.Data.DataTableReader DataReader
        {
            get
            {


                System.Data.DataTableReader Datareadertemp = null;
                try
                {

                    using (MySqlConnection MyCont = new MySqlConnection(ConnectionString))
                    {

                        using (MySqlCommand Mycommandt = new MySqlCommand())
                        {
                            MyCont.Open();
                            for (int x = 0; x < _paramname.Count; x++)
                            {
                                Mycommandt.Parameters.AddWithValue(_paramname[x].ToString(), _paramvalue[x]);
                            }
                            Mycommandt.CommandText = SetSql;
                            Mycommandt.CommandTimeout = 360;
                            Mycommandt.Connection = MyCont;
                            DataSet _dataset = new DataSet();
                            MySqlDataAdapter yadapter = new MySqlDataAdapter(Mycommandt);
                            yadapter.Fill(_dataset);
                            Datareadertemp = _dataset.CreateDataReader();

                        }

                    }

                }
                catch (MySqlException mysqlerror)
                {
                    Error = "Error Code=" + mysqlerror.ErrorCode + linebreak + "Message =" +
                        mysqlerror.Message + linebreak + "Source=" + mysqlerror.Source + linebreak + mysqlerror.StackTrace;
                    IsError = true;


                }
                catch (Exception ex)
                {
                    Error = " Error (is not Mysql error) Message =" +
                        ex.Message + linebreak + "Source=" + ex.Source;
                    IsError = true;


                }
                finally
                {

                }

                return Datareadertemp;
            }




        }
        public int TotalRows
        {

            get
            {
                var m1 = "";
                if (_totRows == 0 && !_totRowscounted)
                {
                    try
                    {
                        using (MySqlConnection MyCont = new MySqlConnection(ConnectionString))
                        {

                            MyCont.Open();
                            using (MySqlCommand Mycommandt = new MySqlCommand())
                            {

                                for (int x = 0; x < _paramname.Count; x++)
                                {
                                    Mycommandt.Parameters.AddWithValue(_paramname[x].ToString(), _paramvalue[x]);
                                }
                                string newsql = SetSql;
                                Mycommandt.CommandText = newsql;
                                Mycommandt.CommandTimeout = 360;
                                Mycommandt.Connection = MyCont;
                                MySqlDataAdapter ad = new MySqlDataAdapter(Mycommandt);

                                DataTable dt = new DataTable("temp");

                                _totRows = ad.Fill(dt);

                            }
                            MyCont.Close();
                        }


                    }
                    catch (MySqlException mysqlerror)
                    {
                        Error = m1 + " ?Error Code=" + mysqlerror.ErrorCode + linebreak + "Message =" +
                            mysqlerror.Message + linebreak + "Source=" + mysqlerror.Source + linebreak + mysqlerror.StackTrace;
                        IsError = true;


                    }
                    catch (Exception ex)
                    {
                        Error = m1 + " ?Error (is not Mysql error) Message =" +
                            ex.Message + linebreak + "Source=" + ex.Source;
                        IsError = true;


                    }
                    finally
                    {
                        _totRowscounted = true;

                    }
                }


                return _totRows;
            }

        }
        public int FieldCount
        {
            get
            {
                if (_totFields == 0)
                {
                    OpenReader();

                }
                return _totFields;
            }


        }
        public string FielType(int posi)
        {
            string mreturn = "";
            if (_totFields == 0)
            {
                OpenReader();
            }
            if (!IsError)
            {

                try
                {
                    mreturn = _fieldsType[posi];

                }
                catch
                {
                    _iserror = true;
                    Error = "(FielType) Field position is incorrect";


                }
            }

            return mreturn;
        }

        private bool OpenReader()
        {

            bool mreturn = true;
            if (_reader == null)
            {
                try
                {
                    if (MyCon != null)
                    {
                        MyCon.Close();

                    }
                    MyCon = new MySqlConnection(ConnectionString);
                    MySqlCommand MyCommand = new MySqlCommand();
                    for (int x = 0; x < _paramname.Count; x++)
                    {
                        MyCommand.Parameters.AddWithValue(_paramname[x].ToString(), _paramvalue[x]);
                    }
                    MyCon.Open();
                    MyCommand.CommandText = SetSql;
                    MyCommand.CommandTimeout = 360;
                    MyCommand.Connection = MyCon;


                    _reader = MyCommand.ExecuteReader();

                    _totFields = _reader.FieldCount;
                    if (_totFields > 0)
                    {
                        _fields = new string[_totFields];
                        _fieldsType = new string[_totFields];
                        for (int x = 0; x < _totFields; x++)
                        {
                            _fields[x] = _reader.GetName(x).ToString();
                            _fieldsType[x] = _reader.GetFieldType(x).ToString().Replace("System.", ""); ;
                        }

                    }

                    mreturn = true;

                }
                catch (MySqlException mysqlerror)
                {
                    mreturn = false;
                    Error = "Error Code=" + mysqlerror.ErrorCode + linebreak + "Message =" +
                        mysqlerror.Message + linebreak + "Source=" + linebreak + mysqlerror.StackTrace + mysqlerror.Source + linebreak + SqlInfo;
                    IsError = true;

                    ClearMysql();
                }


                catch (Exception ex)
                {
                    mreturn = false;

                    Error = "(sec Openreder) Error Code=" + "Message =" +
                             ex.Message + linebreak + " Source=" + ex.Source;
                    IsError = true;
                    ClearMysql();


                }



            }
            else
            {
                mreturn = true;
            }


            return mreturn;
        }
        public bool Read()
        {
           


            bool mreturn = false;
          
            if (OpenReader())
            {
                try
                {
                    mreturn = _reader.Read();

                }
                catch (MySqlException mysqlerror)
                {

                    Error = Error + linebreak + "Read() Error Code=" + mysqlerror.ErrorCode + linebreak + "Message =" +
                             mysqlerror.Message + linebreak + " Source=" + mysqlerror.Source + linebreak + mysqlerror.StackTrace;
                    IsError = true;
                    ClearMysql();


                }
                catch (Exception ex)
                {
                    Error = " (Not mysql error) " + "Message =" +
                             ex.Message + linebreak + " Source=" + ex.Source;
                    IsError = true;
                    ClearMysql();

                }

            }
          


            return mreturn;

        }
        public void ResetRead()
        {
            ReadReset();

        }
        public void ReadReset()
        {
            ClearMysql();

        }

        public string this[int index]
        {
            get { return Read(index); }
        }
        public string this[string index]
        {
            get { return Read(index); }
        }


        public string Read(int mfielpos)
        {
            string mreturn;
            try { mreturn = _reader[mfielpos].ToString(); }
            catch { mreturn = ""; }
            return mreturn;
        }
        public string Read(string mfieldname)
        {
            string mreturn = "";


            try { mreturn = _reader[mfieldname].ToString(); }
            catch
            {
                mreturn = "";

            }
            return mreturn;
        }
        public bool ReadIsNull(string mfieldname)
        {
            bool mreturn;
            try { mreturn = Convert.IsDBNull(_reader[mfieldname]); }
            catch { mreturn = true; }
            return mreturn;
        }
        public string ReadS(string mfieldname)
        {
            string mreturn;
            try { mreturn = _reader[mfieldname].ToString(); }
            catch { mreturn = ""; }
            return mreturn;
        }
        public string ReadS(int mfielpos)
        {
            string mreturn;
            try { mreturn = _reader[mfielpos].ToString(); }
            catch { mreturn = ""; }
            return mreturn;
        }
        public double ReadDouble(string mfieldname)
        {
            double mreturn;
            try { mreturn = _reader.GetDouble(mfieldname); }
            catch { mreturn = 0; }
            return mreturn;
        }
        public double ReadDouble(int mfielpos)
        {
            double mreturn;
            try { mreturn = _reader.GetDouble(mfielpos); }
            catch { mreturn = 0; }
            return mreturn;
        }
        public byte[] ReadByte(string mfieldname)
        {
            byte[] mreturn;
            try { mreturn = (byte[])_reader[mfieldname]; }
            catch { mreturn = null; }
            return mreturn;
        }
        public byte[] ReadByte(int mfielpos)
        {
            byte[] mreturn;
            try { mreturn = (byte[])_reader[mfielpos]; }
            catch { mreturn = null; }
            return mreturn;
        }
        public int ReadI(string mfieldname)
        {
            int mreturn;
            try { mreturn = _reader.GetInt32(mfieldname); }
            catch { mreturn = 0; }
            return mreturn;
        }
        public int ReadI(int mfielpos)
        {
            int mreturn;
            try { mreturn = _reader.GetInt32(mfielpos); }
            catch { mreturn = 0; }
            return mreturn;
        }
        public Int64 ReadI64(string mfieldname)
        {
            Int64 mreturn;
            try { mreturn = _reader.GetInt64(mfieldname); }
            catch { mreturn = 0; }
            return mreturn;
        }
        public Int64 ReadI64(int mfielpos)
        {
            Int64 mreturn;
            try { mreturn = _reader.GetInt64(mfielpos); }
            catch { mreturn = 0; }
            return mreturn;
        }
        public DateTime ReadDate(string mfieldname)
        {

            DateTime mreturn;
            try { mreturn = _reader.GetDateTime(mfieldname); }
            catch { mreturn = new DateTime(1900, 1, 1, 0, 0, 0); }
            return mreturn;
        }
        public DateTime ReadDate(int mfielpos)
        {

            DateTime mreturn;
            try { mreturn = _reader.GetDateTime(mfielpos); }
            catch { mreturn = new DateTime(1900, 1, 1, 0, 0, 0); }
            return mreturn;
        }

        public string ReadDateMysql(string mfieldname)
        {

            string mreturn = string.Format("{0:yyyy-MM-dd HH:mm:ss}", new DateTime(1900, 1, 1, 0, 0, 0));
            try
            {
                mreturn = string.Format("{0:yyyy-MM-dd HH:mm:ss}", _reader.GetDateTime(mfieldname));
            }
            catch { }
            return mreturn;
        }
        public string ReadDateMysql(int mfielpos)
        {

            string mreturn = string.Format("{0:yyyy-MM-dd HH-mm-ss}", new DateTime(1900, 1, 1, 0, 0, 0));
            try
            {
                mreturn = string.Format("{0:yyyy-MM-dd HH-mm-ss}", _reader.GetDateTime(mfielpos));
            }
            catch { }
            return mreturn;
        }
        public string ReadDateSP(string mfieldname)
        {

            string mreturn = string.Format("{0:dd/MM/yyyy HH:mm:ss}", new DateTime(1900, 1, 1, 0, 0, 0));
            try
            {
                mreturn = string.Format("{0:dd/MM/yyyy HH:mm:ss}", _reader.GetDateTime(mfieldname));
            }
            catch { }
            return mreturn;
        }
        public string ReadDateSP(int mfielpos)
        {

            string mreturn = string.Format("{0:yyyy-MM-dd HH-mm-ss}", new DateTime(1900, 1, 1, 0, 0, 0));
            try
            {
                mreturn = string.Format("{0:yyyy-MM-dd HH-mm-ss}", _reader.GetDateTime(mfielpos));
            }
            catch { }
            return mreturn;
        }
        public string ReadDateUSA(string mfieldname)
        {

            string mreturn = string.Format("{0:MM/dd/yyyy hh:mm:ss tt}", new DateTime(1900, 1, 1, 0, 0, 0));
            try
            {
                mreturn = string.Format("{0:MM/dd/yyyy hh:mm:ss tt}", _reader.GetDateTime(mfieldname));
            }
            catch { }
            return mreturn;
        }
        public string ReadDateUSA(int mfielpos)
        {

            string mreturn = string.Format("{0:MM/dd/yyyy hh-mm-ss tt}", new DateTime(1900, 1, 1, 0, 0, 0));
            try
            {
                mreturn = string.Format("{0:MM/dd/yyyy hh-mm-ss tt}", _reader.GetDateTime(mfielpos));
            }
            catch { }
            return mreturn;
        }


        public bool ReadBool(string mfieldname)
        {

            bool mreturn;
            try { mreturn = Convert.ToBoolean(_reader[mfieldname]); }
            catch { mreturn = false; }
            return mreturn;
        }
        public bool ReadBool(int mfielpos)
        {

            bool mreturn;
            try { mreturn = Convert.ToBoolean(_reader[mfielpos]); }
            catch { mreturn = false; }
            return mreturn;
        }
        public char ReadChar(int mfielpos)
        {

            char mreturn;
            try { mreturn = _reader.GetChar(mfielpos); }
            catch { mreturn = ' '; }
            return mreturn;
        }
        public char ReadChar(string mfieldname)
        {

            char mreturn;
            try { mreturn = _reader.GetChar(mfieldname); }
            catch { mreturn = ' '; }
            return mreturn;
        }

        public bool Execute(string thesqlstatement)
        {

            bool mresult = false;
            MySqlConnection MyContemp = new MySqlConnection(ConnectionString);
            MySqlCommand MyCommand = new MySqlCommand();
            try
            {

                MyContemp.Open();
                MyCommand.CommandText = thesqlstatement;
                MyCommand.Connection = MyContemp;
                _rowafected = MyCommand.ExecuteNonQuery();

                mresult = true;
            }
            catch (MySqlException e)
            {

                Error = mresult + linebreak + e.Message + linebreak + e.Source + linebreak + e.StackTrace
                    + linebreak + e.StackTrace + linebreak + thesqlstatement;
                _iserror = true;

            }
            catch (Exception e)
            {

                Error = mresult + linebreak + e.Message + linebreak + e.Source + " General exeption" + linebreak + thesqlstatement;
                _iserror = true;

            }
            finally
            {
                if (MyContemp != null)
                {
                    MyContemp.Close();

                    MyContemp.Dispose();
                    MyContemp = null;
                }

            }

            return mresult;
        }



    } //end ySQLE 
    sealed public class ySQLU : ybase
    {
        public override void Dispose()
        {



        }
        private int _rowafected = 0;
        public int rowsAffected { get { return _rowafected; } private set { _rowafected = value; } }
        public int RegistrosAfectados { get { return _rowafected; } private set { _rowafected = value; } }

        private int _paranum = 1;
        private int _paranumfield = 1;
        string merror = "";
        private ArrayList _paramname = new ArrayList();
        private ArrayList _paramvalue = new ArrayList();
        private string sqlfields = "";
        private string sqlwhere = "";

        private string GetSymbol(yOperator mvalue)
        {
            string mreturn = "=";
            switch (mvalue)
            {
                case yOperator.Less:
                    mreturn = "<";
                    break;
                case yOperator.Bigger:
                    mreturn = ">";
                    break;
                case yOperator.LessEqual:
                    mreturn = "<=";
                    break;
                case yOperator.BiggerEqual:
                    mreturn = ">=";
                    break;
                case yOperator.NotEqual:
                    mreturn = "!=";
                    break;
                case yOperator.IsNull:
                    mreturn = "!=";
                    break;
            }
            return mreturn;

        }
        private bool _iserror = false;
        public bool IsError
        {
            get
            {
                return _iserror;
            }
            private set
            {
                _iserror = value;
            }
        }
        public string Error
        {
            get
            {
                return merror;
            }
            private set
            {
                merror = value;
            }
        }

        public int RowAffected
        { get { return _rowafected; } private set { _rowafected = value; } }

        public ySQLU(string ConnectionStringValue, string mtablename, ySQLP mlist)
            : this(ConnectionStringValue, mtablename)
        {
            for (int x = 0; x < mlist.Count; x++)
            {
                AddUpdate(mlist.Fields[x].ToString(), mlist.Values[x]);
            }
        }

        public ySQLU(string ConnectionStringValue, string mtablename)
        {
            ConnectionString = ConnectionStringValue;
            sqlfields = "Update " + mtablename + " Set ";

            sqlwhere = "";

        }
        private void AddPara<T>(string mparaname, T mvalue)
        {
            _paramname.Add(mparaname);
            _paramvalue.Add(mvalue);
        }

        public void AddUpdate<T>(string mfieldname, T mvalue)
        {
            string mparaname = "?" + mfieldname + "paran" + _paranumfield.ToString();
            sqlfields += (_paramname.Count > 0 ? "," : "") + mfieldname + "=" + mparaname;
            AddPara(mparaname, mvalue);
            _paranumfield++;
        }
        public void AddwhereRaw(string mwhere)
        {
            // this is to allow to put raw in the where
            // ex1 = AddwhereRaw(" and ( id Is Null ) ");
            // ex2 = AddwhereRaw(" or ( id Is not Null ) ");
            // ex3 = AddwhereRaw(" and ( mname like '%ber% ) ");

            //Remainder : This class cannot execute with out parameters so if you put only AddwhereRaw is not going to work
            // also remember use this affter  "AddWhere" if is posible to be safe
            sqlwhere += mwhere;


        }
        public void AddWhere<T>(string mfield, yOperator boosymbol, T mvalue)
        {
            string mmpara = "?p" + mfield + _paranum++.ToString();
            sqlwhere = " (" + mfield + GetSymbol(boosymbol) + mmpara + ") ";


            AddPara(mmpara, mvalue);

        }
        public void AddWhere<T>(string mfield, string boosymbol, T mvalue)
        {
            string mmpara = "?p" + mfield + _paranum++.ToString();
            sqlwhere = " (" + mfield + " " + boosymbol + " " + mmpara + ") ";
            AddPara(mmpara, mvalue);
        }
        public void AddWhere<T>(string mfield, T mvalue)
        {
            AddWhere(mfield, yOperator.Equal, mvalue);
        }
        public void AddWhereAnd<T>(string mfield, yOperator boosymbol, T mvalue)
        {
            string mmpara = "?p" + mfield + _paranum++.ToString();
            if (sqlwhere.Length < 2)
            {
                //avoid to use this if the AddWhereParameter was not use first
                return;
            }
            sqlwhere += " and (" + mfield + GetSymbol(boosymbol) + mmpara + ") ";
            AddPara(mmpara, mvalue);
        }
        public void AddWhereAnd<T>(string mfield, string boosymbol, T mvalue)
        {
            string mmpara = "?p" + mfield + _paranum++.ToString();
            if (sqlwhere.Length < 2)
            {
                //avoid to use this if the AddWhereParameter was not use first
                return;
            }
            sqlwhere += " and (" + mfield + boosymbol + mmpara + ") ";
            AddPara(mmpara, mvalue);
        }
        public void AddWhereAnd<T>(string mfield, T mvalue)
        {
            AddWhereAnd(mfield, yOperator.Equal, mvalue);
        }
        public void AddWhereOr<T>(string mfield, yOperator boosymbol, T mvalue)
        {
            string mmpara = "?p" + mfield + _paranum++.ToString();
            if (sqlwhere.Length < 2)
            {

                return;
            }
            sqlwhere += " or (" + mfield + GetSymbol(boosymbol) + mmpara + ") ";
            AddPara(mmpara, mvalue);
        }
        public void AddWhereOr<T>(string mfield, string boosymbol, T mvalue)
        {
            string mmpara = "?p" + mfield + _paranum++.ToString();
            if (sqlwhere.Length < 2)
            {

                return;
            }
            sqlwhere += " or (" + mfield + boosymbol + mmpara + ") ";
            AddPara(mmpara, mvalue);
        }
        public void AddWhereOr<T>(string mfield, T mvalue)
        {
            AddWhereOr(mfield, yOperator.Equal, mvalue);
        }

        public string SqlInfo
        {
            get
            {
                string mreturn = sqlfields + " where (" + sqlwhere + ")" + linebreak;

                for (int x = 0; x < _paramname.Count; x++)
                {
                    mreturn += @"Paramenten Name=" + _paramname[x].ToString() + ", Value= " +
                        _paramvalue[x].ToString() + " , Type=" + _paramvalue[x].GetType() + linebreak;
                }

                return mreturn;
            }

        }



        public bool Execute()
        {
           
            if (sqlwhere.Length == 0)
            {
                merror = "Please spycify where option";
                return _iserror;

            }
            if (_paramvalue.Count == 0)
            {
                merror = "No fields to update";
                return _iserror;
            }
            string msqlfinal = sqlfields + " where  (" + sqlwhere + ")";

            try
            {
                using (MySqlConnection MyCon = new MySqlConnection(ConnectionString))
                {
                    MyCon.Open();
                    using (MySqlCommand MyCommand = new MySqlCommand())
                    {
                        for (int x = 0; x < _paramvalue.Count; x++)
                        {
                            MyCommand.Parameters.AddWithValue(_paramname[x].ToString(), _paramvalue[x]);
                        }
                        MyCommand.Connection = MyCon;
                        MyCommand.CommandText = msqlfinal;
                        _rowafected = MyCommand.ExecuteNonQuery();
                       

                    }
                }

            }
            catch (MySqlException ex)
            {
                merror = "Message :" + ex.Message + linebreak + " ErrorCode :" + ex.ErrorCode + linebreak + ex.StackTrace;
                _iserror = true;

            }
            finally
            {


            }
            return _iserror;
        }


    }  //end ySQLU
    sealed public class ySQLI : ybase
    {
        public override void Dispose()
        {


        }
        private int _rowafected = 0;
        public int rowsAffected { get { return _rowafected; } private set { _rowafected = value; } }
        public int RegistrosAfectados { get { return _rowafected; } private set { _rowafected = value; } }
        private string merror = "";

        int mlastid = 0;
        private int mparanum = 1;
        ArrayList _paramname = new ArrayList();
        ArrayList _paramvalue = new ArrayList();
        string _tableName = "";

        String sqlvalue1 = "";
        String sqlvalue2 = ") Values (";

        private bool _iserror = false;
        public bool IsError
        {
            get
            {
                return _iserror;
            }
        }

        public string Error
        {
            get
            {
                return merror;
            }
            private set { merror = value; }
        }
        public int LasidInserted
        {
            get
            {
                return mlastid;
            }
        }
        public ySQLI(string ConnectionStringValue, string mtablename, ySQLP mlist)
            : this(ConnectionStringValue, mtablename)
        {
            for (int x = 0; x < mlist.Count; x++)
            {

                Insert(mlist.Fields[x].ToString(), mlist.Values[x]);
            }
        }
        public ySQLI(string ConnectionStringValue, string mtablename)
        {
            ConnectionString = ConnectionStringValue;
            sqlvalue1 = "Insert into " + mtablename + " ( ";
            _tableName = mtablename;

        }
        public void Insert<T>(string mfield, T mvalue)
        {
            string mparaname = "?" + mfield + "yp" + mparanum++.ToString();
            sqlvalue1 += mfield + ",";
            sqlvalue2 += mparaname + ",";
            _paramname.Add(mparaname);
            _paramvalue.Add(mvalue);
        }
        public void Add<T>(string mfield, T mvalue) // same as Insert
        {
            Insert(mfield, mvalue);
        }



        public string SqlInfo
        {
            get
            {

                string mreturn = "SQL statement:" + linebreak + SqlData + linebreak;
                string mtemp = SqlData;
                for (int x = 0; x < _paramname.Count; x++)
                {
                    mreturn += linebreak + linebreak + _paramname[x].ToString() + "= " + _paramvalue[x].ToString() + " ( " + _paramvalue[x].GetType() + " )";
                    mtemp = mtemp.Replace(_paramname[x].ToString(), " " + _paramvalue[x].ToString() + " ");
                }
                mreturn += linebreak + linebreak + "Final SQL :" + mtemp + linebreak;
                return mreturn;
            }
        }

        private string SqlData
        {
            get
            {
                string mreturn = "";
                // remove the last , from the strings and put )
                string tmp1 = sqlvalue1.Substring(0, sqlvalue1.Length - 1) + " ";
                string tmp2 = sqlvalue2.Substring(0, sqlvalue2.Length - 1) + " ) ";
                mreturn = tmp1 + tmp2;
                return mreturn;
            }
        }
        private bool _reuseId = false;
        private string _idnamefield = "id";

        public bool ReuseId
        {
            set { _reuseId = value; }
            get { return _reuseId; }

        }
        public string ReuseIdFieldName
        {
            set { _idnamefield = value; }
            get { return _idnamefield; }
        }


        private int GetUnusedid()
        {
            //this get the first unused id number 
            //the purpose is to use the id unused 

            int mreturn = 0;
            string msql = @"SELECT  #id#
                    FROM    (
                            SELECT  1 AS #id#
                            ) q1
                    WHERE   NOT EXISTS
                            (
                            SELECT  1
                            FROM   #thetable#
                            WHERE   #id# =?pid
                            )
                    UNION ALL
                    SELECT  *
                    FROM    (
                            SELECT #id# + 1
                            FROM    #thetable# t
                            WHERE   NOT EXISTS
                                    (
                                    SELECT  1
                                    FROM    #thetable# ti
                                    WHERE   ti.#id# = t.#id# + 1
                                    )
                            ORDER BY
                                    #id#
                            LIMIT 1
                            ) q2
                    ORDER BY
                            #id#
                    LIMIT 1";
            msql = msql.Replace("#id#", _idnamefield);
            msql = msql.Replace("#thetable#", _tableName);
            ySQLE mexe = new ySQLE(ConnectionString, msql, "?pid", 1);
            if (mexe.Read())
            {
                mreturn = mexe.ReadI(_idnamefield);

            }
            if (mexe.IsError)
            {
                mreturn = 0;
            }
            return mreturn;



        }
        public int Execute()
        {
           
          

            try
            {
               

                using (MySqlConnection MyCon = new MySqlConnection(ConnectionString))
                {
                    using (MySqlCommand MyCommand = new MySqlCommand())
                    {
                        for (int x = 0; x < _paramvalue.Count; x++)
                        {
                            MyCommand.Parameters.AddWithValue(_paramname[x].ToString(), _paramvalue[x]);
                        }
                        MyCommand.CommandText = SqlData;
                        MyCon.Open();
                        MyCommand.Connection = MyCon;
                        _rowafected = MyCommand.ExecuteNonQuery();


                        string msql2 = "Select LAST_INSERT_ID()";
                        MySqlCommand MyCommand2 = new MySqlCommand(msql2, MyCon);
                        MySqlDataReader _reader = MyCommand2.ExecuteReader();
                        if (_reader.Read())
                        {
                            mlastid = Convert.ToInt32(_reader[0].ToString());
                        }

                        _reader.Close();

                    }
                }



            }
            catch (MySqlException ex)
            {
                mlastid = 0;
                _iserror = true;
                merror = ex.Message + linebreak + ex.Source + linebreak + ex.Data + linebreak + ex.ErrorCode + linebreak + ex.InnerException + linebreak + ex.ToString() + linebreak + ex.StackTrace;
            }
            finally
            {



            }
            return mlastid;
        }
    } //end ySQLI
    sealed public class ySQLP : ybase
    {
        private ArrayList mfields;
        private ArrayList mvalues;
        private ArrayList mtype;
        public override void Dispose()
        {


        }
        public ySQLP()
        {
            mfields = new ArrayList();
            mvalues = new ArrayList();
            mtype = new ArrayList();

        }
        public string Info
        {
            get
            {
                string mreturn = "";
                for (int x = 0; x < mfields.Count; x++)
                {
                    mreturn += "Field #" + x.ToString() + " =" + mfields[x] + " value =" + mvalues[x] + " Type =" + mvalues[x].GetType() + linebreak + linebreak;
                }
                if (mreturn == "")
                {
                    mreturn = "No Field added yet";
                }
                return mreturn;

            }

        }
        public void Add<T>(string mfield, T mvalue)
        {
            mfields.Add(mfield);
            mvalues.Add(mvalue);
            string temptype = mvalue.GetType().ToString().ToLower().Replace("system.", "");
            mtype.Add(temptype);

        }
        public void AddUpdate<T>(string mfield, T mvalue) // same as Add only to guarantie the safe use with the class ySQLI
        {
            Add(mfield, mvalue);
        }
        public void Insert<T>(string mfield, T mvalue) // same as Add only to guarantie the safe use with the class ySQLU
        {
            Add(mfield, mvalue);
        }
        public int Count
        {
            get
            {
                return mfields.Count;
            }
        }
        public string ParamName(int x)
        {
            return mfields[x].ToString();
        }
        public ArrayList Fields
        {
            get
            {

                return mfields;
            }
        }
        public ArrayList Values
        {
            get
            {
                return mvalues;
            }
        }
        public ArrayList Types
        {
            get
            {
                return mtype;
            }
        }

    }
    sealed public class ySQLIM : ybase
    {
        private int _rowafected = 0;
        public int rowsAffected { get { return _rowafected; } private set { _rowafected = value; } }
        public int RegistrosAfectados { get { return _rowafected; } private set { _rowafected = value; } }
        private string _error = "";
        private string SqlData = "";
        private string _timebeggin = "";
        private string _timeend = "";
        public string TimeStart { get { return _timebeggin; } }
        public string TimeEnd { get { return _timeend; } }

        public int RecordAffected { get { return _rowafected; } }

        public bool IsError { get { return Error.Length > 1; } }
        List<string> _paramname = new List<string>();
        ArrayList _paramvalue = new ArrayList();
        public string Error
        {
            get
            {


                return _error;
            }
            private set
            {
                _error = value;



            }
        }
        public override void Dispose()
        {


        }
        public bool Execute()
        {
            if (SqlData == "")
            {
                Error = "No ingreso campos ni datos";

            }
            _timebeggin = DateTime.Now.ToString();

            try
            {
                using (MySqlConnection MyCon = new MySqlConnection(ConnectionString))
                {
                    using (MySqlCommand MyCommand = new MySqlCommand())
                    {
                        for (int x = 0; x < _paramvalue.Count; x++)
                        {
                            MyCommand.Parameters.AddWithValue(_paramname[x].ToString(), _paramvalue[x]);
                        }
                        try
                        {
                            MyCommand.CommandText = SqlData;
                            MyCon.Open();
                            MyCommand.Connection = MyCon;
                            try
                            {
                                _rowafected = MyCommand.ExecuteNonQuery();
                                _timeend = DateTime.Now.ToString();
                            }
                            catch (Exception ex)
                            {


                                Error = ex.Message + linebreak + ex.Source + linebreak + ex.Data + linebreak + "\r\n" + SqlInfo;
                            }
                            finally
                            {
                                try
                                {

                                    MyCon.Close();

                                }
                                catch { }

                            }
                            return IsError;

                        }
                        catch (MySqlException ex)
                        {


                            Error = ex.Message + linebreak + ex.Source + linebreak + ex.Data + linebreak + ex.ErrorCode + linebreak + ex.InnerException + linebreak + ex.ToString() + linebreak + ex.StackTrace;
                        }
                        finally
                        {
                            try
                            {

                                MyCon.Close();

                            }
                            catch { }

                        }
                        return IsError;

                    }
                }



            }
            catch (MySqlException ex)
            {


                Error = ex.Message + linebreak + ex.Source + linebreak + ex.Data + linebreak + ex.ErrorCode + linebreak + ex.InnerException + linebreak + ex.ToString() + linebreak + ex.StackTrace;
            }
            finally
            {



            }
            return IsError;


        }

        public ySQLIM(string mconection, string mtable, List<string> mfields, List<string> mvalues)
        {

            ConnectionString = mconection;

            if (mvalues == null || mvalues.Count == 0)
            {

                Error = "lista de valores vacios";


            }
            if (mfields == null || mfields.Count == 0)
            {

                Error += "lista de campos vacio";


            }
            if (IsError) { return; }
            if ((mvalues.Count % mfields.Count) != 0)
            {

                Error = "No coiciden cantidad de valores y campos   valores/campos no debe tener residuos";
                return;

            }

            _paramname.Clear();
            _paramvalue.Clear();


            SqlData = "insert into " + mtable;
            string msqlvals = "";
            string mtemp = "";
            for (int x = 0; x < mfields.Count; x++)
            {

                mtemp += (mtemp == "" ? "" : ",") + mfields[x];
            }
            SqlData += "(" + mtemp + ") values ";


            int mcountvals = 0;

            while (mcountvals < mvalues.Count)
            {
                mtemp = "";

                for (int x = 0; x < mfields.Count; x++)
                {

                    string mparanametmp = "?parayapo" + mcountvals;

                    _paramname.Add(mparanametmp);
                    _paramvalue.Add(mvalues[mcountvals]);
                    mtemp += (mtemp == "" ? "" : ",") + mparanametmp;
                    mcountvals++;
                }
                msqlvals += (msqlvals == "" ? "" : ",") + "(" + mtemp + ")";
            }
            SqlData += msqlvals;











        }

        public string SqlInfo
        {
            get
            {
                string mreturn = "";

                mreturn += "Comando sql :" + SqlData + "\r\n Parametros y valores :\r\n";
                for (int x = 0; x < _paramname.Count; x++)
                {
                    mreturn += _paramname[x] + " = " + _paramvalue[x] + "\r\n";


                }

                return mreturn;

            }
        }


    }
    sealed public class yTableDat : ybase
    {
        private List<string> _nameFields = new List<string>();
        private List<string> _typeMysql = new List<string>();
        private List<string> _typeNet = new List<string>();
        private List<string> _typeNumericScale = new List<string>();

        public List<string> NameField { get { return _nameFields; } }
        public List<string> TypeMysql { get { return _typeMysql; } }
        public List<string> TypeNet { get { return _typeNet; } }

        public string CreateClass
        {
            get
            {
                StringBuilder mprops = new StringBuilder();
                for (int x = 0; x < NameField.Count; x++)
                {
                    string mtempname = NameField[x].Substring(0, 1).ToUpper() + NameField[x].Substring(1).ToLower();


                    mprops.Append("\t\tpublic " + TypeNet[x] + " " + mtempname + " {get;set;}\r\n");


                }
                return mprops.ToString();


            }

        }



        public override void Dispose()
        {


        }

        public yTableDat(string ConnectionStringValue, string Tablename)
        {

            string msql = @"SELECT TABLE_NAME
       , COLUMN_NAME
       , DATA_TYPE
       , CHARACTER_MAXIMUM_LENGTH
       , CHARACTER_OCTET_LENGTH 
       , NUMERIC_PRECISION 
       , NUMERIC_SCALE AS SCALE
       , COLUMN_DEFAULT
       , IS_NULLABLE
       FROM INFORMATION_SCHEMA.COLUMNS where  TABLE_NAME=?pname";
            using (ySQLE mexe = new ySQLE(ConnectionStringValue, msql, "?pname", Tablename))
            {
                while (mexe.Read())
                {
                    _nameFields.Add(mexe["COLUMN_NAME"]);
                    _typeMysql.Add(mexe["DATA_TYPE"]);
                    switch (mexe["DATA_TYPE"].ToLower())
                    {
                        case "varchar":
                            TypeNet.Add("string");

                            break;
                        case "text":
                            TypeNet.Add("string");

                            break;
                        case "mediumtext":
                            TypeNet.Add("string");

                            break;

                        case "int":
                            TypeNet.Add("int"); break;

                        case "tinyint":
                            TypeNet.Add("int"); break;
                        case "smallint":
                            TypeNet.Add("int"); break;

                        case "double":
                            TypeNet.Add("double");

                            break;
                        case "binary":
                            TypeNet.Add("byte[]"); break;

                        case "datetime":
                            TypeNet.Add("DateTime");

                            break;
                        case "date":
                            TypeNet.Add("DateTime");

                            break;
                        default:
                            TypeNet.Add("none"); break;

                    }


                }
            }

        }
    }

    public class ySQLLIVE
    {
        ~ySQLLIVE()
        {


        }
        private string _error = "";
        public string Error { get { return _error; } private set { _error = value; } }
        public bool IsError { get { return Error.Length > 1; } }
        private string ConnectionString = "";

        public ySQLLIVE(string ConnectionStringValue)
        {
            ConnectionString = ConnectionStringValue;
            MySqlConnection MyContemp = new MySqlConnection(ConnectionString);
            MyContemp.Open();
        }



        #region SQLE

        public class ySQLE : ybase
        {

            private int _errcount = 0;
            public MySqlDataReader _reader;
            private ArrayList _errors = new ArrayList();
            private string[] _fields;
            private string[] _fieldsType;
            private int _totFields = 0;//reset this in the main costructor;
            private int _totRows = 0;//reset this in the main costructor;

            private bool _totRowscounted = false;
            MySqlConnection MyCon;//reset this in the main costructor;
            private ArrayList _paramname = new ArrayList();
            private ArrayList _paramvalue = new ArrayList();


            private string msqlcommand;

            private static string staticmerror = "";
            private string _error;
            private bool _iserror = false;

            public bool HasRows
            {
                get
                {

                    if (_reader == null)
                    {
                        if (!OpenReader())
                        {
                            return false;
                        }

                    }
                    return _reader.HasRows;
                }

            }
            public string[] Fields
            {

                get
                {
                    if (_totFields == 0)
                    {
                        OpenReader();

                    }
                    return _fields;
                }

            }

            public string[] FieldsTypes
            {

                get
                {
                    if (_totFields == 0)
                    {
                        OpenReader();

                    }
                    return _fieldsType;
                }

            }

            public string FieldType(string mname)
            {
                string mreturn =
                    "";
                if (_totFields == 0)
                {
                    OpenReader();

                }

                for (int x = 0; x < _fields.Length; x++)
                {
                    if (_fields[x].ToString().ToLower() == mname.ToLower())
                    {
                        mreturn = _fieldsType[x].ToString();
                    }
                }
                return mreturn;

            }
            public string FieldType(int mposition)
            {
                string mreturn =
                    "";
                if (_totFields == 0)
                {
                    OpenReader();

                }
                try
                {
                    mreturn = _fieldsType[mposition].ToString();
                }

                catch
                {

                }

                return mreturn;

            }
            public string FieldName(int mposition)
            {
                string mreturn =
                    "";
                if (_totFields == 0)
                {
                    OpenReader();

                }
                try
                {
                    mreturn = _fields[mposition].ToString();
                }
                catch
                {

                }

                return mreturn;

            }
            public bool IsError
            {
                get
                {
                    return _iserror;
                }
                private set { _iserror = value; }
            }
            public string Error
            {
                get
                {
                    string mtemp = "";
                    foreach (string mmerror in _errors)
                    {
                        mtemp += mmerror + linebreak + linebreak;
                    }

                    return mtemp;
                }
                private set
                {
                    _error = value;
                    if (value.Length > 2)
                    {
                        _errcount++;
                        _errors.Add(" (Err # " + _errcount.ToString() + " )" + value);
                    }


                }
            }
            public string Info
            {
                /// <summary>
                /// Description for SomeMethod.</summary>

                get
                {
                    string mreturn = "";
                    string mbreak = linebreak + "============>" + linebreak;
                    mreturn += string.Format("{0:MM/dd/yyyy HH:mm:ss}", DateTime.Now) + mbreak;
                    mreturn += "ERROR =" + (IsError ? "yes" : "No") + mbreak;
                    if (IsError)
                    {
                        mreturn += Error + mbreak;

                    }
                    else
                    {
                        mreturn += SqlInfo + mbreak;

                    }
                    mreturn += "DATABASE =";
                    mreturn += "CONNECTIONSTRING =" + ConnectionString;
                    mreturn += "CONNECTION TO DATABASE = ";
                    if (MyCon != null)
                    {
                        mreturn += MyCon.State + mbreak;
                    }
                    else
                    {
                        mreturn += "Closed/null" + mbreak;
                    }
                    mreturn += "READER = ";

                    if (_reader != null)
                    {
                        mreturn += _reader.IsClosed ? "Closed" : "Open";
                        if (!_reader.IsClosed)
                        {
                            mreturn += ", HASROW=" + _reader.HasRows.ToString() + ", TOTALFIELD =" + _totFields.ToString() + ", TOTALROWS =" + TotalRows.ToString() + mbreak;
                            int xcount = 0;

                            if (_totFields > 0)
                            {

                                mreturn += "FIELD LIST" + mbreak;
                                foreach (string mfield in _fields)
                                {
                                    mreturn += linebreak + "Field #" + xcount++.ToString() + " = " + mfield.ToUpper() + " , Type =" + FieldType(mfield);

                                }


                            }

                        }
                        mreturn += linebreak;
                    }
                    else
                    {
                        mreturn += "Closed/null";
                        mreturn += ", TotalRows =" + TotalRows.ToString();
                    }




                    return mreturn;
                }

            }
            public int ErrorCount
            {
                get { return _errcount; }
            }
            public string[] Errors
            {
                get
                {
                    string[] _errstring2 = new string[_errors.Count];
                    for (int x = 0; x < _errors.Count; x++)
                    {
                        _errstring2[x] = " (Error #" + (x + 1).ToString() + " ) =" + _errors[x].ToString() + linebreak;
                    }
                    return _errstring2;
                }
            }


            public static string ErrorStatic
            {
                get
                {

                    return staticmerror;
                }

            }
            public bool IsokConnection
            {
                get
                {
                    bool mreturn = true;

                    MySqlConnection mcontest = new MySqlConnection(ConnectionString);
                    try
                    {
                        mcontest.Open();
                    }

                    catch (Exception ex)
                    {
                        staticmerror = ex.Message;
                        mreturn = false;

                    }
                    finally
                    {
                        mcontest.Close();
                        mcontest = null;


                    }
                    return mreturn;
                }
            }

            //use this to do with multiple parameter

            public string SetSql
            {
                get
                {
                    return msqlcommand;
                }
                set
                {
                    msqlcommand = value;

                }
            }
            public ySQLE()
            {

            }
            public ySQLE(string ConnectionStringValue)
            {
                ConnectionString = ConnectionStringValue;
            }

            public ySQLE(string ConnectionStringValue, string sqlvalue, ySQLP mlist)
                : this()
            {
                ConnectionString = ConnectionStringValue;
                SetSql = sqlvalue;
                for (int x = 0; x < mlist.Count; x++)
                {
                    AddParameter(mlist.Fields[x].ToString(), mlist.Values[x]);
                }
                ;
            }

            public ySQLE(string ConnectionStringValue, string sqlvalue)
                : this()
            {
                ConnectionString = ConnectionStringValue;
                SetSql = sqlvalue;

            }
            //this is overloaded with one paramenter
            public ySQLE(string ConnectionStringValue, string sqlvalue, string parameter1, string value1)
                : this()
            {
                ConnectionString = ConnectionStringValue;
                SetSql = sqlvalue;
                AddParameter(parameter1, value1);

            }
            public ySQLE(string ConnectionStringValue, string sqlvalue, string parameter1, int value1)
                : this()
            {
                ConnectionString = ConnectionStringValue; SetSql = sqlvalue;
                AddParameter(parameter1, value1);

            }

            public ySQLE(string ConnectionStringValue, string sqlvalue, string parameter1, DateTime value1)
                : this()
            {
                ConnectionString = ConnectionStringValue; SetSql = sqlvalue;
                AddParameter(parameter1, value1);

            }
            public ySQLE(string ConnectionStringValue, string sqlvalue, string parameter1, byte[] value1)
                : this()
            {
                ConnectionString = ConnectionStringValue; SetSql = sqlvalue;
                AddParameter(parameter1, value1);

            }
            public ySQLE(string ConnectionStringValue, string sqlvalue, string parameter1, double value1)
                : this()
            {
                ConnectionString = ConnectionStringValue; SetSql = sqlvalue;
                AddParameter(parameter1, value1);

            }


            //this is overloaded with two paramenter
            public ySQLE(string ConnectionStringValue, string sqlvalue, string parameter1, string value1, string parameter2, string value2)
                : this()
            {
                ConnectionString = ConnectionStringValue; SetSql = sqlvalue;
                AddParameter(parameter1, value1);
                AddParameter(parameter2, value2);
            }

            public ySQLE(string ConnectionStringValue, string sqlvalue, string parameter1, string value1, string parameter2, int value2)
                : this()
            {
                ConnectionString = ConnectionStringValue; SetSql = sqlvalue;
                AddParameter(parameter1, value1);
                AddParameter(parameter2, value2);
            }
            public ySQLE(string ConnectionStringValue, string sqlvalue, string parameter1, int value1, string parameter2, int value2)
                : this()
            {
                ConnectionString = ConnectionStringValue; SetSql = sqlvalue;
                AddParameter(parameter1, value1);
                AddParameter(parameter2, value2);
            }
            public ySQLE(string ConnectionStringValue, string sqlvalue, string parameter1, string value1, string parameter2, DateTime value2)
                : this()
            {
                ConnectionString = ConnectionStringValue; SetSql = sqlvalue;
                AddParameter(parameter1, value1);
                AddParameter(parameter2, value2);
            }
            public ySQLE(string ConnectionStringValue, string sqlvalue, string parameter1, int value1, string parameter2, DateTime value2)
                : this()
            {
                ConnectionString = ConnectionStringValue; SetSql = sqlvalue;
                AddParameter(parameter1, value1);
                AddParameter(parameter2, value2);
            }

            public ySQLE(string ConnectionStringValue, string sqlvalue, string parameter1, string value1, string parameter2, string value2, string parameter3, string value3)
                : this()
            {
                ConnectionString = ConnectionStringValue; SetSql = sqlvalue;
                AddParameter(parameter1, value1);
                AddParameter(parameter2, value2);
                AddParameter(parameter3, value3);
            }
            public ySQLE(string ConnectionStringValue, string sqlvalue, string parameter1, string value1, string parameter2, int value2, string parameter3, string value3)
                : this()
            {
                ConnectionString = ConnectionStringValue; SetSql = sqlvalue;
                AddParameter(parameter1, value1);
                AddParameter(parameter2, value2);
                AddParameter(parameter3, value3);
            }
            public ySQLE(string ConnectionStringValue, string sqlvalue, string parameter1, string value1, string parameter2, int value2, string parameter3, int value3)
                : this()
            {
                ConnectionString = ConnectionStringValue; SetSql = sqlvalue;
                AddParameter(parameter1, value1);
                AddParameter(parameter2, value2);
                AddParameter(parameter3, value3);
            }
            public ySQLE(string ConnectionStringValue, string sqlvalue, string parameter1, int value1, string parameter2, int value2, string parameter3, int value3)
                : this()
            {
                ConnectionString = ConnectionStringValue; SetSql = sqlvalue;
                AddParameter(parameter1, value1);
                AddParameter(parameter2, value2);
                AddParameter(parameter3, value3);
            }
            public ySQLE(string ConnectionStringValue, string sqlvalue, string parameter1, string value1, string parameter2, string value2, string parameter3, string value3, string parameter4, string value4)
                : this()
            {
                ConnectionString = ConnectionStringValue;
                SetSql = sqlvalue;
                AddParameter(parameter1, value1);
                AddParameter(parameter2, value2);
                AddParameter(parameter3, value3);
                AddParameter(parameter4, value4);
            }

            public void Limit(int mlimitvalue)
            {
                SetSql = SetSql + " Limit " + mlimitvalue.ToString();

            }
            public void Limit(int mlimitvalue1, int mlimitvalue2)
            {
                SetSql = SetSql + " Limit " + mlimitvalue1.ToString() + "," + mlimitvalue2.ToString();

            }

            public void AddParameter<T>(string mparamname, T mvalue)
            {

                if (mparamname.Substring(0, 1) != "?")
                {
                    mparamname = "?" + mparamname;
                }
                _paramname.Add(mparamname);
                _paramvalue.Add(mvalue);

            }

            //this is to get the whole information of the job to perform and I use to save in the error table 
            public string SqlInfo
            {
                get
                {
                    string mreturn = linebreak + "SQL Command :" + linebreak + SetSql + linebreak + "Paramerters:";
                    for (int x = 0; x < _paramvalue.Count; x++)
                    {
                        mreturn += linebreak + "name =" + _paramname[x].ToString() + " , value=" + _paramvalue[x].ToString();
                    }
                    return mreturn;
                }
            }

            public bool Execute()
            {

                bool mresult = false;
                try
                {
                    MySqlConnection MyContemp = new MySqlConnection(ConnectionString);

                    MySqlCommand MyCommand = new MySqlCommand();

                    for (int x = 0; x < _paramname.Count; x++)
                    {
                        MyCommand.Parameters.AddWithValue(_paramname[x].ToString(), _paramvalue[x]);
                    }

                    if (MyCommand.Parameters.Count > 0)
                    {
                        MyContemp.Open();
                        MyCommand.CommandText = SetSql;
                        MyCommand.Connection = MyContemp;
                        MyCommand.ExecuteNonQuery();
                        MyCommand.CommandText = "";
                        MyCommand.Connection = null;


                        mresult = true;

                    }
                    else
                    {
                        Error = "Cannot use this with out parameters";
                        _iserror = true;
                    }
                    MyContemp.Close();
                    MyContemp.Dispose();
                    MyContemp = null;




                }
                catch (MySqlException e)
                {

                    Error = mresult + linebreak + e.Message + linebreak + e.Source + linebreak + e.StackTrace
                        + linebreak + e.StackTrace + linebreak + SqlInfo;
                    _iserror = true;

                }
                catch (Exception e)
                {

                    Error = mresult + linebreak + e.Message + linebreak + e.Source;
                    _iserror = true;

                }

                return mresult;
            }


            public int Rows
            {
                get
                {
                    return TotalRows;
                }
            }

            public System.Data.DataTableReader DataReader
            {
                get
                {


                    System.Data.DataTableReader Datareadertemp = null;
                    try
                    {

                        using (MySqlConnection MyCont = new MySqlConnection(ConnectionString))
                        {

                            using (MySqlCommand Mycommandt = new MySqlCommand())
                            {
                                MyCont.Open();
                                for (int x = 0; x < _paramname.Count; x++)
                                {
                                    Mycommandt.Parameters.AddWithValue(_paramname[x].ToString(), _paramvalue[x]);
                                }
                                Mycommandt.CommandText = SetSql;
                                Mycommandt.CommandTimeout = 360;
                                Mycommandt.Connection = MyCont;
                                DataSet _dataset = new DataSet();
                                MySqlDataAdapter yadapter = new MySqlDataAdapter(Mycommandt);
                                yadapter.Fill(_dataset);
                                Datareadertemp = _dataset.CreateDataReader();

                            }

                        }

                    }
                    catch (MySqlException mysqlerror)
                    {
                        Error = "Error Code=" + mysqlerror.ErrorCode + linebreak + "Message =" +
                            mysqlerror.Message + linebreak + "Source=" + mysqlerror.Source + linebreak + mysqlerror.StackTrace;
                        IsError = true;


                    }
                    catch (Exception ex)
                    {
                        Error = " Error (is not Mysql error) Message =" +
                            ex.Message + linebreak + "Source=" + ex.Source;
                        IsError = true;


                    }
                    finally
                    {

                    }

                    return Datareadertemp;
                }




            }
            public int TotalRows
            {

                get
                {
                    if (_totRows == 0 && !_totRowscounted)
                    {
                        try
                        {
                            using (MySqlConnection MyCont = new MySqlConnection(ConnectionString))
                            {

                                MyCont.Open();
                                using (MySqlCommand Mycommandt = new MySqlCommand())
                                {

                                    for (int x = 0; x < _paramname.Count; x++)
                                    {
                                        Mycommandt.Parameters.AddWithValue(_paramname[x].ToString(), _paramvalue[x]);
                                    }
                                    string newsql = "Select count(*) as t1 " + SetSql.Substring((SetSql.IndexOf("from", StringComparison.OrdinalIgnoreCase)));
                                    Mycommandt.CommandText = newsql;
                                    Mycommandt.CommandTimeout = 360;
                                    Mycommandt.Connection = MyCont;

                                    MySqlDataReader _readertmp = Mycommandt.ExecuteReader();
                                    if (_readertmp.Read())
                                    {
                                        _totRows = Convert.ToInt32(_readertmp["t1"]);

                                    }
                                    _readertmp.Close();

                                }
                                MyCont.Close();
                            }


                        }
                        catch (MySqlException mysqlerror)
                        {
                            Error = "Error Code=" + mysqlerror.ErrorCode + linebreak + "Message =" +
                                mysqlerror.Message + linebreak + "Source=" + mysqlerror.Source + linebreak + mysqlerror.StackTrace;
                            IsError = true;


                        }
                        catch (Exception ex)
                        {
                            Error = " Error (is not Mysql error) Message =" +
                                ex.Message + linebreak + "Source=" + ex.Source;
                            IsError = true;


                        }
                        finally
                        {
                            _totRowscounted = true;

                        }
                    }


                    return _totRows;
                }

            }
            public int FieldCount
            {
                get
                {
                    if (_totFields == 0)
                    {
                        OpenReader();

                    }
                    return _totFields;
                }


            }
            public string FielType(int posi)
            {
                string mreturn = "";
                if (_totFields == 0)
                {
                    OpenReader();
                }
                if (!IsError)
                {

                    try
                    {
                        mreturn = _fieldsType[posi];

                    }
                    catch
                    {
                        _iserror = true;
                        Error = "(FielType) Field position is incorrect";


                    }
                }

                return mreturn;
            }

            private bool OpenReader()
            {

                bool mreturn = true;
                if (_reader == null)
                {
                    try
                    {
                        if (MyCon != null)
                        {
                            MyCon.Close();

                        }
                        MyCon = new MySqlConnection(ConnectionString);
                        MySqlCommand MyCommand = new MySqlCommand();
                        for (int x = 0; x < _paramname.Count; x++)
                        {
                            MyCommand.Parameters.AddWithValue(_paramname[x].ToString(), _paramvalue[x]);
                        }
                        MyCon.Open();
                        MyCommand.CommandText = SetSql;
                        MyCommand.CommandTimeout = 360;
                        MyCommand.Connection = MyCon;


                        _reader = MyCommand.ExecuteReader();
                        _totFields = _reader.FieldCount;
                        if (_totFields > 0)
                        {
                            _fields = new string[_totFields];
                            _fieldsType = new string[_totFields];
                            for (int x = 0; x < _totFields; x++)
                            {
                                _fields[x] = _reader.GetName(x).ToString();
                                _fieldsType[x] = _reader.GetFieldType(x).ToString().Replace("System.", ""); ;
                            }

                        }

                        mreturn = true;

                    }
                    catch (MySqlException mysqlerror)
                    {
                        mreturn = false;
                        Error = "Error Code=" + mysqlerror.ErrorCode + linebreak + "Message =" +
                            mysqlerror.Message + linebreak + "Source=" + linebreak + mysqlerror.StackTrace + mysqlerror.Source + linebreak + SqlInfo;
                        IsError = true;


                    }


                    catch (Exception ex)
                    {
                        mreturn = false;

                        Error = "(sec Openreder) Error Code=" + "Message =" +
                                 ex.Message + linebreak + " Source=" + ex.Source;
                        IsError = true;



                    }



                }
                else
                {
                    mreturn = true;
                }


                return mreturn;
            }
            public bool Read()
            {

                bool mreturn = false;
                if (OpenReader())
                {
                    try
                    {
                        mreturn = _reader.Read();

                    }
                    catch (MySqlException mysqlerror)
                    {

                        Error = Error + linebreak + "Read() Error Code=" + mysqlerror.ErrorCode + linebreak + "Message =" +
                                 mysqlerror.Message + linebreak + " Source=" + mysqlerror.Source + linebreak + mysqlerror.StackTrace;
                        IsError = true;



                    }
                    catch (Exception ex)
                    {
                        Error = " (Not mysql error) " + "Message =" +
                                 ex.Message + linebreak + " Source=" + ex.Source;
                        IsError = true;


                    }

                }
                return mreturn;

            }
            public void ResetRead()
            {
                ReadReset();

            }
            public void ReadReset()
            {


            }

            public string this[int index]
            {
                get { return Read(index); }
            }
            public string this[string index]
            {
                get { return Read(index); }
            }


            public string Read(int mfielpos)
            {
                string mreturn;
                try { mreturn = _reader[mfielpos].ToString(); }
                catch { mreturn = ""; }
                return mreturn;
            }
            public string Read(string mfieldname)
            {
                string mreturn;
                try { mreturn = _reader[mfieldname].ToString(); }
                catch { mreturn = ""; }
                return mreturn;
            }
            public bool ReadIsNull(string mfieldname)
            {
                bool mreturn;
                try { mreturn = Convert.IsDBNull(_reader[mfieldname]); }
                catch { mreturn = true; }
                return mreturn;
            }
            public string ReadS(string mfieldname)
            {
                string mreturn;
                try { mreturn = _reader[mfieldname].ToString(); }
                catch { mreturn = ""; }
                return mreturn;
            }
            public string ReadS(int mfielpos)
            {
                string mreturn;
                try { mreturn = _reader[mfielpos].ToString(); }
                catch { mreturn = ""; }
                return mreturn;
            }
            public double ReadDouble(string mfieldname)
            {
                double mreturn;
                try { mreturn = _reader.GetDouble(mfieldname); }
                catch { mreturn = 0; }
                return mreturn;
            }
            public double ReadDouble(int mfielpos)
            {
                double mreturn;
                try { mreturn = _reader.GetDouble(mfielpos); }
                catch { mreturn = 0; }
                return mreturn;
            }
            public byte[] ReadByte(string mfieldname)
            {
                byte[] mreturn;
                try { mreturn = (byte[])_reader[mfieldname]; }
                catch { mreturn = null; }
                return mreturn;
            }
            public byte[] ReadByte(int mfielpos)
            {
                byte[] mreturn;
                try { mreturn = (byte[])_reader[mfielpos]; }
                catch { mreturn = null; }
                return mreturn;
            }
            public int ReadI(string mfieldname)
            {
                int mreturn;
                try { mreturn = _reader.GetInt32(mfieldname); }
                catch { mreturn = 0; }
                return mreturn;
            }
            public int ReadI(int mfielpos)
            {
                int mreturn;
                try { mreturn = _reader.GetInt32(mfielpos); }
                catch { mreturn = 0; }
                return mreturn;
            }
            public Int64 ReadI64(string mfieldname)
            {
                Int64 mreturn;
                try { mreturn = _reader.GetInt64(mfieldname); }
                catch { mreturn = 0; }
                return mreturn;
            }
            public Int64 ReadI64(int mfielpos)
            {
                Int64 mreturn;
                try { mreturn = _reader.GetInt64(mfielpos); }
                catch { mreturn = 0; }
                return mreturn;
            }
            public DateTime ReadDate(string mfieldname)
            {

                DateTime mreturn;
                try { mreturn = _reader.GetDateTime(mfieldname); }
                catch { mreturn = new DateTime(1900, 1, 1, 0, 0, 0); }
                return mreturn;
            }
            public DateTime ReadDate(int mfielpos)
            {

                DateTime mreturn;
                try { mreturn = _reader.GetDateTime(mfielpos); }
                catch { mreturn = new DateTime(1900, 1, 1, 0, 0, 0); }
                return mreturn;
            }

            public string ReadDateMysql(string mfieldname)
            {

                string mreturn = string.Format("{0:yyyy-MM-dd HH:mm:ss}", new DateTime(1900, 1, 1, 0, 0, 0));
                try
                {
                    mreturn = string.Format("{0:yyyy-MM-dd HH:mm:ss}", _reader.GetDateTime(mfieldname));
                }
                catch { }
                return mreturn;
            }
            public string ReadDateMysql(int mfielpos)
            {

                string mreturn = string.Format("{0:yyyy-MM-dd HH-mm-ss}", new DateTime(1900, 1, 1, 0, 0, 0));
                try
                {
                    mreturn = string.Format("{0:yyyy-MM-dd HH-mm-ss}", _reader.GetDateTime(mfielpos));
                }
                catch { }
                return mreturn;
            }
            public string ReadDateSP(string mfieldname)
            {

                string mreturn = string.Format("{0:dd/MM/yyyy HH:mm:ss}", new DateTime(1900, 1, 1, 0, 0, 0));
                try
                {
                    mreturn = string.Format("{0:dd/MM/yyyy HH:mm:ss}", _reader.GetDateTime(mfieldname));
                }
                catch { }
                return mreturn;
            }
            public string ReadDateSP(int mfielpos)
            {

                string mreturn = string.Format("{0:yyyy-MM-dd HH-mm-ss}", new DateTime(1900, 1, 1, 0, 0, 0));
                try
                {
                    mreturn = string.Format("{0:yyyy-MM-dd HH-mm-ss}", _reader.GetDateTime(mfielpos));
                }
                catch { }
                return mreturn;
            }
            public string ReadDateUSA(string mfieldname)
            {

                string mreturn = string.Format("{0:MM/dd/yyyy hh:mm:ss tt}", new DateTime(1900, 1, 1, 0, 0, 0));
                try
                {
                    mreturn = string.Format("{0:MM/dd/yyyy hh:mm:ss tt}", _reader.GetDateTime(mfieldname));
                }
                catch { }
                return mreturn;
            }
            public string ReadDateUSA(int mfielpos)
            {

                string mreturn = string.Format("{0:MM/dd/yyyy hh-mm-ss tt}", new DateTime(1900, 1, 1, 0, 0, 0));
                try
                {
                    mreturn = string.Format("{0:MM/dd/yyyy hh-mm-ss tt}", _reader.GetDateTime(mfielpos));
                }
                catch { }
                return mreturn;
            }


            public bool ReadBool(string mfieldname)
            {

                bool mreturn;
                try { mreturn = Convert.ToBoolean(_reader[mfieldname]); }
                catch { mreturn = false; }
                return mreturn;
            }
            public bool ReadBool(int mfielpos)
            {

                bool mreturn;
                try { mreturn = Convert.ToBoolean(_reader[mfielpos]); }
                catch { mreturn = false; }
                return mreturn;
            }
            public char ReadChar(int mfielpos)
            {

                char mreturn;
                try { mreturn = _reader.GetChar(mfielpos); }
                catch { mreturn = ' '; }
                return mreturn;
            }
            public char ReadChar(string mfieldname)
            {

                char mreturn;
                try { mreturn = _reader.GetChar(mfieldname); }
                catch { mreturn = ' '; }
                return mreturn;
            }

            public bool Execute(string thesqlstatement)
            {

                bool mresult = false;
                MySqlConnection MyContemp = new MySqlConnection(ConnectionString);
                MySqlCommand MyCommand = new MySqlCommand();
                try
                {

                    MyContemp.Open();
                    MyCommand.CommandText = thesqlstatement;
                    MyCommand.Connection = MyContemp;
                    MyCommand.ExecuteNonQuery();

                    mresult = true;
                }
                catch (MySqlException e)
                {

                    Error = mresult + linebreak + e.Message + linebreak + e.Source + linebreak + e.StackTrace
                        + linebreak + e.StackTrace + linebreak + thesqlstatement;
                    _iserror = true;

                }
                catch (Exception e)
                {

                    Error = mresult + linebreak + e.Message + linebreak + e.Source + " General exeption" + linebreak + thesqlstatement;
                    _iserror = true;

                }
                finally
                {
                    if (MyContemp != null)
                    {
                        MyContemp.Close();

                        MyContemp.Dispose();
                        MyContemp = null;
                    }

                }

                return mresult;
            }



        } //end ySQLE 
        #endregion




    }

   








}