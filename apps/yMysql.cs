﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YapoMysqlTool.apps
{
    public abstract class yMysql
    {
        protected string connectionString { get; set; }
        protected MySqlConnection mysqlConnection { get; set; }
        protected MySqlCommand MyCommand { get; set; }
        public List<Parameter> Parameters { get; set; }

        protected string msqlcommand { get; set; }


        protected void Open()
        {
            mysqlConnection = new MySqlConnection(connectionString);
            MyCommand = new MySqlCommand();
            foreach (var item in Parameters)
            {
                string paramname = item.Name;
                string paramvalue = item.value;
                if (paramname.Substring(0, 1) != "?") { paramname = "?" + paramname; }
                MyCommand.Parameters.AddWithValue(paramname, paramvalue);

            }
            MyCommand.CommandText = msqlcommand;
            mysqlConnection.Open();
         
        }


    }
    public class Parameter
    {
        public string Name { get; set; }
      
        public string value { get; set; }
        

    }
}
